<img src="https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/logo.png" alt="MagSnow" width="150"/>

# Guide d'utilisation

## Avant propos
Ce document présente les différentes fonctionnalités du portail MagSnow. Pour une vue globale de l'application, se reporter à la [vidéo de présentation](./assets/video_presentation.mwv).

## Sommaire
* Visualiser la qualité de la neige
* Tracer son itinéraire
* Gérer les différentes couches
* Rechercher un lieu ou un massif
* S'informer sur les avalanches

## Visualiser la qualité de la neige

Le premier objectif du portail est de présenter aux utilisateurs la qualité de la neige sur une zone de leur choix, à une certaine date là aussi choisie (par défaut celle du jour). Il est possible de visualiser les données brutes en sélectionnant la couche correspondante (expliqué plus loin dans ce guide), mais ces données sont difficilements lisibles. Elles permettent néanmoins de savoir si on acquisition satellite a eu lieu le jour choisi par l'utilisateur. Si la date choisie ne contient pas d'acquisition satellite, un message d'erreur s'affiche en dessous.

Vous avez le choix entre visualiser la couche de neige fraîche et de neige humide sous forme vectorielle, en cliquant dans le menu (1) sur `Analyse de la neige fraîche` ou `Analyse de la neige humide`, puis en selectionant la zone souhaitée avec `alt+click`.

![AnalyseNeige](https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/aide_neige.png "Outil d'analyse de neige").


⚠️ **Il est nécessaire de sélectionner une zone de taille raisonnable** (10km*10km), sans quoi le calcul risque d'être long. Dans ce cas , le calcul ne pourra pas être réalisé et l'utilisateur sera averti via un message en rouge dans la console d'analyse de la neige. ⚠️

La couche se charge directement sur la carte. Vous pouvez ensuite supprimer cette couche avec les boutons de suppression. La répartition s'affiche également dans le menu.

⚠️ **Vous ne pouvez pas analyser la neige si vous avez cliqué sur `Tracer mon itinéraire`**. Vous devez appuyer sur `Masquer mon itinéraire` avant. ⚠️ 

![ResultatNeige](https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/aide_quali_neige.png "Résultat de l'analyse").
  
## Tracer son itinéraire

Tracez votre itinéraire en cliquant sur le bouton `Tracer mon itinéraire`, puis en cliquant sur la carte autant de point que souhaité. Double cliquez pour arrêter le tracé. Le profil s'affiche dans le menu. La précision est celle proposée par l'IGN, de l'ordre du m dans les zones montagneuses.

![traceiti](https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/aide_trace.png "Outil de tracé d'itinéraire").


Le profil s'affiche alors dans le menu. Si vous faite passer votre itinéraire dans des zones de neige fraîche précédemment calculées, ces dernières s'affichent également dans le profil sous forme de points blancs.

![profil](https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/aide_profil.png "Profil altimétrique").

⚠️ Comme indiqué, le profil altimétrique ne fonctionne qu'en France. ⚠️ 

## Gérer les différentes couches

Vous pouvez choisir les couches que vous souhaiter visualiser. Le choix des couches s'effectue à droite de l'écran, au niveau des 3 premières bulles. 
   * La première bulle (1) contient les fonds de carte [IGN-Topo](https://www.geoportail.gouv.fr/donnees/carte-topographique-ign) (a) et [OpenTopoMap](https://opentopomap.org/) (b).
   * La deuxième bulle (2) contient les données satellite (données Neige brutes (c) ([plus d'infos sur ces données](https://land.copernicus.eu/pan-european/biophysical-parameters/high-resolution-snow-and-ice-monitoring/snow-products/snow-state-conditions)) et images satellite (d)
   * La troisième bulle (3) contient les sentiers OSM (e), la carte des pentes de l'IGN (f), et les massifs montagneux français et pyrénéens espagnol (g)

![Couches](https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/aide_couches.png "Les données disponibles à l'affichage").

Le gestionnaire de couche en haut de l'écran vous permet de : 
   * sélectionner l'opacité de la couche via la barre d'opacité (1)
   * supprimer la couche (2)
   * voir la légende des couches sur les pentes et les données brutes de Neige (3)

![GestionCouches](https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/aide_gestion_couches.png "Outil pour gérer les couches").

## Rechercher un lieu ou un massif

La barre de recherche situé en haut à droite de l'écran vous permet d'entrer une localité française afin d'amener la carte à l'endroit souhaité. 
Juste au dessus de la barre se situe un sélecteur qui permet de passer des communes aux massifs dans la recherche. Dans chaque cas, `cliquez sur la loupe` après avoir sélectionné le lieu souhaité. 

## S'informer sur les avalanches 

Deux données sont disponibles concernant les avalanches; celle du risque avalanche de [Météo France](https://meteofrance.com/meteo-montagne) (1), et celle des avalanches passées de [data-avalanche.org](http://www.data-avalanche.org) (2). 

![Avalanches](https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/aide_avalanches.png "S'informer sur les avalanches").

Lorsque vous cliquez sur le bouton 1, les massifs apparaissent à l'écran. Vous pouvez alors sélectionner un massif en cliquant dessus. Le bulletin de risque avalanche (BRA) apparaît. Pour revenir à la carte et choisir un autre massif, appuyez sur la croix. Pour fermer le choix du massif, appuyez sur revenir à la carte.

![BRA](https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/aide_BRA.png "Affichage du BRA sur le massif voulu").

Lorsque vous cliquez sur le bouton 2, les avalanches passées apparaissent à l'écran sous forme de carré rouge. Vous pouvez alors en cliquant dessus visualiser les données sur cette avalanche passée.

## Auteurs

* **Maxime Bonnet** [Max_Bon](https://gitlab.com/Max_Bon)
* **Vincent Heau** [vincent.heau](https://gitlab.com/vincent.heau)
* **Lubin Roineau** [lubin_roineau](https://gitlab.com/lubin_roineau)
* **Jacqueline Williams** [Jackgeo](https://gitlab.com/Jackgeo)

<a href="https://www.ensg.eu/">
         <img alt="ENSG" src="https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/logo_ENSG.png" height="100">
</a>
<a href="https://www.magellium.com/fr/">
         <img alt="Magellium" src="https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/Logo_Magellium.png" height="100">
</a>
<a href="https://www.ign.fr">
         <img alt="IGN" src="https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/logo_IGN.png" height="100">
</a>
