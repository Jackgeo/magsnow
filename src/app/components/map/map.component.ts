/** Map.component.ts
 * Code des fonctionnalités du composant Map
 * 
 * Lubin ROINEAU , Vincent HEAU , Jacqueline WILLIAMS, Maxime BONNET 
 * Mai 2022
 */

/* Imports */
import { Component, OnInit } from '@angular/core';
import { fromLonLat } from 'ol/proj';


import { MapService } from 'src/app/services/map.service';
import View from 'ol/View';
import VectorLayer from 'ol/layer/Vector';
import Icon from 'ol/style/Icon';
import TileLayer from 'ol/layer/Tile';

import {xml2json} from 'xml-js';
import VectorSource from 'ol/source/Vector';
import {GeoJSON} from 'ol/format';
import Point from 'ol/geom/Point';
import Feature from 'ol/Feature';

import {Fill, Stroke, Style} from 'ol/style';
import { TileWMS } from 'ol/source';

/* Variables globales servant à empêcher une action de se produire lors du clic sur la carte */
let activite = true; // Pour le bulletin d'avalanche
let activite_risque = true ; // Pour les risques d'avalanche


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

/** Class Map Component
 * Permet la gestion de l'affichage des couches sur la carte
 * Gestion des deux boutons d'avalanche (bulletin & anciennes avalanches)
 */
export class MapComponent implements OnInit {
  
  // Les différentes couches 
  public IGNTOPO;
  public OPENTOPO;
  public WDS;
  public SWS;
  public S2L2A;
  public PENTE;
  public MASSIF;
  
  // Les compteurs (0 ou 1) permettant l'activation et la désactivation des couches
  public compteur_igntopo;
  public compteur_opentopo;
  public compteur_wds;
  public compteur_s2l2a;
  public compteur_sentier;
  public compteur_pente;
  public compteur_massif;
  
  // Date à sélectionner pour la couche WDS
  public dateNeige : Date;


  /**
     * Constructeur de la classe MapComponent
    */
  constructor(private mapService: MapService) { 
    
    this.IGNTOPO = this.mapService.IGNTOPO;
    this.OPENTOPO = this.mapService.OPENTOPO;
    this.WDS = this.mapService.WDS;
    this.SWS = this.mapService.SWS;
    this.S2L2A = this.mapService.S2L2A;
    this.PENTE = this.mapService.PENTE;
    this.MASSIF = this.mapService.MASSIF;

    this.compteur_igntopo = 1;
    this.compteur_opentopo = 0;
    this.compteur_wds = 0;
    this.compteur_s2l2a = 0;

    this.compteur_sentier = 0;
    this.compteur_pente = 1;
    this.compteur_massif = 0;

    this.dateNeige =new Date();

  }

  ngOnInit(): void {
    // Initialisation de la carte principale lors du chargement de la page
    this.mapService.initMap();   
  };

  ////////////////////////////////////////////////////////////
  //   Partie 1 : Gestion de l'affichage sur le portail     //
  ////////////////////////////////////////////////////////////

  /**
     * Fonction permettant de mettre à jour la couche WDS lors d'un changement dans les paramètres 
     * réalisé par les utilisateurs
  */
  maj_wds(){
    
    this.mapService.map.removeLayer(this.mapService.WDS);

    // Paramètre : date
    var jour = this.dateNeige.getDate();
    var mois = this.dateNeige.getMonth()+1;
    var annee = this.dateNeige.getFullYear();

    var date_choisie = annee + "-" + mois + "-" + jour;
    
    this.mapService.WDS =  new TileLayer({
      source: new TileWMS({
        url :'https://cryo.land.copernicus.eu/wms/WDS/',
        crossOrigin: 'anonymous',
        params: {'LAYERS': '1_SSC',
                  'TIME': date_choisie},
      }),
    });

    this.WDS = this.mapService.WDS;
    this.mapService.map.addLayer(this.mapService.WDS);

  }
  
  /**
     * Affichage de la bulle OUTILS de contrôle des couches 
  */
  affichage_outils(){

    //Initialisation: Suppression des autres bulles
    document.getElementById("bulle-fond-carte").style.visibility = "hidden";
    document.getElementById("bulle-sentinel").style.visibility = "hidden";

    if (document.getElementById("bulle-outils").style.visibility == "visible"){
      document.getElementById("bulle-outils").style.visibility = "hidden";
    }
    else{
      document.getElementById("bulle-outils").style.visibility = "visible";
    } 
  }
  
  /**
     * Affichage de la bulle SENTINEL de contrôle des couches 
  */
  affichage_sentinel(){
    

    //Initialisation: Suppression des autres bulles
    document.getElementById("bulle-fond-carte").style.visibility = "hidden";
    document.getElementById("bulle-outils").style.visibility = "hidden";

    if (document.getElementById("bulle-sentinel").style.visibility == "visible"){
      document.getElementById("bulle-sentinel").style.visibility = "hidden";
    }
    else{
      document.getElementById("bulle-sentinel").style.visibility = "visible";
    } 
  }
  
  /**
     * Affichage de la bulle FOND-CARTE de contrôle des couches 
  */
  affichage_fondcarte(){
    

    //Initialisation: Suppression des autres bulles
    document.getElementById("bulle-sentinel").style.visibility = "hidden";
    document.getElementById("bulle-outils").style.visibility = "hidden";

    if (document.getElementById("bulle-fond-carte").style.visibility == "visible"){
      document.getElementById("bulle-fond-carte").style.visibility = "hidden";
    }
    else{
      document.getElementById("bulle-fond-carte").style.visibility = "visible";
    } 
  }

  /**
     * Afficher et Masquer le fond de carte IGN Topographique
     * @param {Number} compteur - Le compteur d'activation ou de désactivation
    */
  fond_igntopo(compteur){
    

    if (compteur%2 == 0){
      this.compteur_igntopo+=1;
      document.getElementById("ign-topo").style.opacity = "1";
      this.mapService.map.addLayer(this.mapService.IGNTOPO);
      // Affichage du réglage de l'opacité
      document.getElementById("reg_ign").style.display = "block";    
      
    }
    else{
      this.compteur_igntopo+=1;
      document.getElementById("ign-topo").style.opacity = "0.5";
      this.mapService.map.removeLayer(this.mapService.IGNTOPO);
      // Suppression du réglage de l'opacité
      document.getElementById("reg_ign").style.display = "none"; 
    } 
  
  }
 

  /**
     * Afficher et Masquer le fond de carte OPENTOPO
     * @param {Number} compteur - Le compteur d'activation ou de désactivation
    */
  fond_opentopo(compteur){
    
    if (compteur%2 == 0){
      this.compteur_opentopo+=1;
      document.getElementById("open-topo").style.opacity = "1";
      this.mapService.map.addLayer(this.mapService.OPENTOPO);
      // Affichage du réglage de l'opacité
      document.getElementById("reg_opentopo").style.display = "block";
    }
    else{
      this.compteur_opentopo+=1;
      document.getElementById("open-topo").style.opacity = "0.5";
      this.mapService.map.removeLayer(this.mapService.OPENTOPO);
      // Suppression du réglage de l'opacité
      document.getElementById("reg_opentopo").style.display = "none";
    } 
  
  }
  
  /**
     * Afficher et Masquer le fond de carte WDS
     * @param {Number} compteur - Le compteur d'activation ou de désactivation
    */
  fond_WDS(compteur){

    if (compteur%2 == 0){
      this.compteur_wds+=1;
      document.getElementById("wetdrysnow").style.opacity = "1";
      this.mapService.map.addLayer(this.mapService.WDS);
      // Affichage du réglage de l'opacité
      document.getElementById("reg_wds").style.display = "block";
    }
    else{
      this.compteur_wds+=1;
      document.getElementById("wetdrysnow").style.opacity = "0.5";
      this.mapService.map.removeLayer(this.mapService.WDS);
      // Suppression du réglage de l'opacité
      document.getElementById("reg_wds").style.display = "none";
    }
  }
  
  /**
     * Afficher et Masquer le fond de carte satellite
     * @param {Number} compteur - Le compteur d'activation ou de désactivation
    */
  fond_S2L2A(compteur){
    
    if (compteur%2 == 0){
      this.compteur_s2l2a+=1;
      document.getElementById("s2l2A").style.opacity = "1";
      this.mapService.map.addLayer(this.mapService.S2L2A);
      // Affichage du réglage de l'opacité
      document.getElementById("reg_s2l2a").style.display = "block";
    
    }
    else{
      this.compteur_s2l2a+=1;
      document.getElementById("s2l2A").style.opacity = "0.5";
      this.mapService.map.removeLayer(this.mapService.S2L2A);
      // Suppression du réglage de l'opacité
      document.getElementById("reg_s2l2a").style.display = "none";
    }
  }
  
  /**
     * Afficher et Masquer le fond de carte sentier OSM
     * @param {Number} compteur - Le compteur d'activation ou de désactivation
    */
  fond_sentier(compteur){

    if (compteur%2 == 0){
      this.compteur_sentier+=1;
      document.getElementById("sentier").style.opacity = "1";
      this.mapService.map.addLayer(this.mapService.SENTIER);
      
    }
    else{
      this.compteur_sentier+=1;
      document.getElementById("sentier").style.opacity = "0.5";
      this.mapService.map.removeLayer(this.mapService.SENTIER);
      
    }
  }
  
  /**
     * Afficher et Masquer le fond de carte pente IGN
     * @param {Number} compteur - Le compteur d'activation ou de désactivation
    */
  fond_pente(compteur){

    if (compteur%2 == 0){
      this.compteur_pente+=1;
      document.getElementById("pente").style.opacity = "1";
      this.mapService.map.addLayer(this.mapService.PENTE);
      // Affichage du réglage de l'opacité
      document.getElementById("reg_pente").style.display = "block";
    }
    else{
      this.compteur_pente+=1;
      document.getElementById("pente").style.opacity = "0.5";
      this.mapService.map.removeLayer(this.mapService.PENTE);
      // Suppression du réglage de l'opacité
      document.getElementById("reg_pente").style.display = "none";
    }
  }
  
  /**
     * Afficher et Masquer le fond de carte massif
     * @param {Number} compteur - Le compteur d'activation ou de désactivation
    */
  fond_massif(compteur){

    if (compteur%2 == 0){
      this.compteur_massif+=1;
      document.getElementById("massif").style.opacity = "1";
      this.mapService.map.addLayer(this.mapService.MASSIF);
    }
    else{
      this.compteur_massif+=1;
      document.getElementById("massif").style.opacity = "0.6";
      this.mapService.map.removeLayer(this.mapService.MASSIF);
    }
  }
  
  /**
     * Fonction permettant de changer l'opacité d'une couche
     * @param {Layer} COUCHE - La couche en question
     * @param {String} nom_div - Le nom de la div qui gère l'opacité de cette couche
    */
  changement_opacite(COUCHE,nom_div){

    var input = (<HTMLInputElement>document.getElementById(nom_div)).value;
    this.mapService.map.removeLayer(COUCHE);
    COUCHE["values_"]["opacity"] = "" + parseFloat(input)/100;
    this.mapService.map.addLayer(COUCHE);
  }
  
  /**
     * Fonction permettant de fermer le réglage d'une couche
     * @param {String} nom_li - Le nom de la div
    */
  fermeture_reg(nom_li){

    (<HTMLInputElement>document.getElementById(nom_li)).style.display = "none";
  }
  
  /**
     * Fonction permettant de fermer les fenêtres sur les avalanches
    */
  fermeture(){
        
    document.getElementById('div-avalanche').style.display = 'none';
    document.getElementById('div-avalanche-semaine').style.display = 'none';
  }

  /**
     * Fonction permettant de fermer les couches sur les avalanches
    */
  retourcarte(){
    
    activite = false;
    activite_risque = false ;
    document.getElementById("div-avalanche").style.display = "none";
    document.getElementById("div-avalanche-semaine").style.display = "none";
    
    // Suppresion des couches ajoutées dont le nom est 'BRA' ou 'Risque_Avalanche'
    for (let i = 0; i < this.mapService.map.getAllLayers().length; i++) {
      
      if (('BRA' == this.mapService.map.getAllLayers()[i].get('name')) || ('Risque_Avalanche' == this.mapService.map.getAllLayers()[i].get('name'))){
        this.mapService.map.removeLayer(this.mapService.map.getAllLayers()[i]);
      }
    }
  }

  
  /////////////////////////////////
  //   Partie 2 : Avalanches     //
  /////////////////////////////////
  
  // ----Partie 2.1 ----
  //-----Bulletin des risques d'avalanche ----
  
  /**
     * Appel du bulletin d'avalanche
    */
  call_BRA(){
    
    this.zone_BRA(this.mapService.map);
  }

  /**
     * Crée le bulletin d'avalanche lors du clic sur le massif choisi
     * @param {Map} map La carte sur laquelle apparaissent les massifs
    */
  zone_BRA(map){
    
    activite =true;
    // Création du style pour les massifs
    var styles = {'MultiPolygon': new Style({
      fill: new Fill({
        color: 'rgba(0,0,0,0)',
      }),
      stroke: new Stroke({
        color: 'burgundy',
        width: 2
      })})}

    var styleFunction = function (feature) {
      return styles[feature.getGeometry().getType()];
    };


    var couche_massif_BRA = new VectorLayer({
      source: new VectorSource({
          url: "https://api.jsonbin.io/b/627f777b019db467969e14be",
          format: new GeoJSON()
          
      }),
      style : styleFunction,
      visible: true
    });
    
    //Ajout des massifs sur la carte
    map.addLayer(couche_massif_BRA);

    //Centrage sur la france entière
    
    var coords = [2.71,46.23];
    var view = new View({
      center: fromLonLat(coords),
      zoom: 6,
    });
    
    map.setView(view);


    couche_massif_BRA.set('name', 'BRA');

    var div_avalanche = document.getElementById("div-avalanche");
    
    var Date = document.getElementById('date');
    var Description = document.getElementById('description');
    
    map.on('click', function(e) {
       
      if(activite){
        map.forEachFeatureAtPixel(e.pixel, function(feature) {

        let BRA = feature.get('code');
        let Nom = feature.get('title');

        if ( BRA.toString().length<=1){
          BRA = '0'+BRA;
        }
 
      var url='https://api.meteofrance.com/files/mountain/bulletins/BRA'+BRA+'.xml' ;

        fetch(url)
        .then(function (result) {
          // retourne le résultat binaire en text
          return result.text();
        })
        .then(function (result) {
          try{
            // Récupération dans le fichier JSON
          var DATE = JSON.parse(xml2json(result))["elements"][1]["elements"][0]["elements"][0]["text"];
          var RISQUE = JSON.parse(xml2json(result))["elements"][1]["elements"][1]["elements"][0]["attributes"]["RISQUEMAXI"];
          var COM_RISQUE = JSON.parse(xml2json(result))["elements"][1]["elements"][1]["elements"][0]["attributes"]["COMMENTAIRE"];
          
  
          feature.setProperties({'date':DATE, 'risque':RISQUE,'com_risque':COM_RISQUE});
          //Mise en forme du résultat
        var risque = feature.get('risque');
          
        document.getElementById("nom-massif").innerHTML= Nom;

        if (risque == undefined) {
          document.getElementById("image").setAttribute('src',"");
          Date.innerHTML = feature.get('date');
          Description.innerHTML = feature.get('com_risque');
          div_avalanche.style.display= "none";
        }

        else {
          document.getElementById("image").setAttribute('src',"./assets/risque"+risque+".png");
          Date.innerHTML = feature.get('date');
          Description.innerHTML = feature.get('com_risque');
          div_avalanche.style.display= "block";
        } 
          }
          catch{
            document.getElementById("nom-massif").innerHTML= '';
            document.getElementById("image").setAttribute('src',"./assets/bonhomme.png");
            Date.innerHTML = "Le bulletin d'avalanche de ce massif n'est pas disponible"
            Description.innerHTML = '';
            div_avalanche.style.display= "block";
          }
        });   
    })
      }
  });

};
    
    
    
// ----Partie 2.1 ----
//-----Avalanches passées ----

  
/**
     * Appel de la fonction recent_avalanche
  */
call_avalanche(){
  
  this.recent_avalanche(this.mapService.map)
}

/**
     * Affichage les avalanches produites lors de la semaine passée
     * @param {Map} map La carte sur laquelle apparaissent les massifs
  */
recent_avalanche(map) {

  activite_risque = true;

  // ERREUR CORS : Solution non-viable, utilisation de thingproxy;
  //var RSS_URL = `https://thingproxy.freeboard.io/fetch/http://data-avalanche.org/feed`;
  var URL_test = "https://api.jsonbin.io/b/6283899625069545a33a4a0f/1"
  
  fetch(URL_test)
  .then(function (result) {
    return result.text();
  })
  .then(function (result) {
      
      let Result = JSON.parse(result);
      //let Result = new DOMParser().parseFromString(result,"text/xml");

      //SOLUTION XML
      //let test = Result.getElementsByTagName("entry");
      const testArray = [];

      let test = Result["feed"]["entry"];
      
      //liste de toutes les avalanches dans le flux RSS
      for (let i = 0; i < test.length; i++) {
          testArray.push(test[i]);
          }
      
      let markers=[];
      // Pour chaque avalanche recuperee
      for (let i = 0; i<=(testArray.length-1); i++){
        
        // Différentes informations
        // let TITLE = testArray[i].childNodes[1].innerHTML;
        // let SUMMARY = testArray[i].childNodes[11].innerHTML;
        // let LONGLAT = [parseFloat(testArray[i].childNodes[17].innerHTML),parseFloat(testArray[i].childNodes[15].innerHTML)];
        // let ORIENTATION =testArray[i].childNodes[19].innerHTML;
        // let ALTITUDE = testArray[i].childNodes[23].innerHTML;
        
        let TITLE = testArray[i]["title"];
        let SUMMARY = testArray[i]["summary"];
        let LONGLAT = [parseFloat(testArray[i]["long"]),parseFloat(testArray[i]["lat"])];
        let ORIENTATION =testArray[i]["orientation"];
        let ALTITUDE = testArray[i]["altitude_start"];
        
        var mark = new Feature({
          geometry: new Point(fromLonLat(LONGLAT))
        });

        mark.setId("marker"+i);
        mark.setProperties({
          'title':TITLE,
          'description':SUMMARY,
          'orientation':ORIENTATION,
          'altitude':ALTITUDE
        });
        mark.setStyle(new Style({
            image:new Icon({
              color: '#BB0B0B',
              crossOrigin: 'anonymous',
              src:'assets/vectorpoint.png',
              imgSize:[15,15]
            })
        })
        ); 
        
        markers.push(mark);
      }
      
      var vectorSource = new VectorSource({
        features: markers
      });

      var vectorLayer = new VectorLayer({
        source: vectorSource
      });
      
      // Ajout des markers sur la carte
      map.addLayer(vectorLayer);

      //Centrage sur la france entière 
      var coords = [2.71,46.23];
      var view = new View({
        center: fromLonLat(coords),
        zoom: 6,
      });
      
      map.setView(view);
      vectorLayer.set('name', 'Risque_Avalanche');

      //Initialisation de la fenêtre
      var Titre = document.getElementById('avalanche-title');
      var Description = document.getElementById('avalanche-description');
      var Orientation = document.getElementById('avalanche-orientation');
      var Altitude = document.getElementById('avalanche-alti');

      map.on('click', function(e) {
        
        if(activite_risque){

          // Massif sur lequel on se trouve
          map.forEachFeatureAtPixel(e.pixel, function(feature) {

            document.getElementById("div-avalanche-semaine").style.display = "block";
            Titre.innerHTML = feature.get('title');
            Description.innerHTML = feature.get('description')
            Orientation.innerHTML = "Orientation de l'avalanche : "+feature.get('orientation')
            Altitude.innerHTML = "Altitude de Départ : "+feature.get('altitude')+'m'

          });
        } 
      });
    });
  }  
}
    

