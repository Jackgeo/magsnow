/** Header.component.ts
 * Code des fonctionnalités incluses dans la barre du header
 * 
 * Lubin ROINEAU , Vincent HEAU , Jacqueline WILLIAMS, Maxime BONNET 
 * Mai 2022
 */

/* Imports */

import { Component, OnInit } from '@angular/core';
import { View } from 'ol';
import { fromLonLat } from 'ol/proj';
import { MapService } from 'src/app/services/map.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

/** Class HeaderComponent
 * Ensemble des méthodes liées au header (barre de recherche)
 * Gestion des deux boutons d'avalanche (bulletin & anciennes avalanches)
 */
export class HeaderComponent implements OnInit {

  constructor(public mapService: MapService ) {}

  ngOnInit(): void {}
  
  /**
     * Recherche une localité à partir des éléments entrées dans la barre de recherche
    */
  recherche_localite(){
    
    var e = <HTMLSelectElement>document.getElementById("localite");
    var strUser = e.value;
    
    // Recherche par communes
    if (parseInt(strUser)==0){

      var data_liste=document.getElementById('ville');
      var localite=document.querySelector('input').value;
      //Appel de l'API
      var url='https://geo.api.gouv.fr/communes?nom='+localite+'&fields=centre,codesPostaux';
      var nom_ville='';
      var str='';

      if(localite.length<2){
        data_liste.innerHTML='';
      }
      // On commence à afficher uniquement au bout d'un certain nombre de lettre pour limiter
      // l'affichage
      else if(localite.length>3){
        fetch(url)
        .then(function (result) {
          // retourne le résultat binaire en text
          return result.text();
        })
        .then(function (result) {
          result=JSON.parse(result);
          
          for (var i=0;i<result.length;i++){
            nom_ville=result[i]['nom'].toString();
            var code_postal=result[i]["codesPostaux"].toString();

            if(code_postal.length>6){
              code_postal=code_postal.substring(0,5);
            }
            str += '<option value="'+nom_ville+' ('+code_postal+')" />';
          }
          data_liste.innerHTML=str;
        });
      }  
    }
    
    // Recherche par massifs 
    else{

      var data_liste=document.getElementById('ville');
      var localite=document.querySelector('input').value;
      localite=localite.charAt(0).toUpperCase() + localite.slice(1);
      var url='Massifs_France.geojson';
      var nom_ville='';
      var str='';

      // On commence à afficher uniquement au bout d'un certain nombre de lettre pour limiter
      // l'affichage
      if(localite.length<2){
        data_liste.innerHTML='';
      }
      if(localite.length>1){
        // Code d'accés, fichier GeoJSOn des massifs en ligne
        //fetch("https://api.jsonbin.io/b/627f777b019db467969e14be")

        //fetch("./assets/Massifs_France.geojson")
        fetch("https://api.jsonbin.io/b/627f777b019db467969e14be")
        .then(function(result){
          return result.text();
        })
        .then(function(result){

          result=JSON.parse(result);
          for (var u=0;u<result["features"].length;u++){
            
            // Validation du massif sélectionné
            if (result["features"][u]["properties"]['title'].indexOf(localite)>-1){
              var possible=result["features"][u]["properties"]['title'];
              str += '<option value= "' +possible + '" />';
            };
          }
          data_liste.innerHTML=str;
        })
          
      }
    }
  }
  

  /**
     * Associe une vue de la carte à la sélection précédente
     * @param{Map} map - La carte en question
    */
  public trouver_coordinate(map){

    var e = <HTMLSelectElement>document.getElementById("localite");
    var strUser = e.value;
    
    // Recherche par communes 
    if (parseInt(strUser)==0){

      var ville=document.querySelector('input').value;
      var vrai_ville=ville.substring(0,ville.length-8);
      var code=ville.substring(ville.length-6,ville.length-1);
      var latitude='';
      var longitude='';
      var url='https://geo.api.gouv.fr/communes?nom='+vrai_ville+'&fields=centre,codesPostaux';
      
      fetch(url)
      .then(function (result) {
        // retourne le résultat binaire en text
        return result.text();
      })
      .then(function (result) {
        result=JSON.parse(result);
        
        for (var i=0;i<result.length;i++){

          if(result[i]['nom'].toString()==vrai_ville && result[i]['codesPostaux'].toString().includes(code)){
            // Récupération des longitudes et latitudes du lieu
            longitude=result[i]['centre']['coordinates'][0];
            latitude=result[i]['centre']['coordinates'][1];
          };
          
          }
          
          if (latitude != '' || longitude != ''){
            
            // Centrage de la carte sur le lieu désiré
            map.setView(new View({
              center: fromLonLat([parseFloat(longitude),parseFloat(latitude)]),
              zoom: 12
            }));

          }
          else{
            window.alert("Veuillez sélectionner une localité");
          }

    })
  }

  // Recherche par communes
  else{

    var ville = document.querySelector('input').value;
    var latitude='';
    var longitude='';

    var url='Massifs_France.geojson';

    //fetch("https://api.jsonbin.io/b/627f777b019db467969e14be")
    fetch("./assets/Massifs_France.geojson")
    .then(function (result) {
      // retourne le résultat binaire en text
      return result.text();
    })
    .then(function (result) {
      result=JSON.parse(result);
      
      for (var i=0;i<result["features"].length;i++){

        if(result["features"][i]["properties"]['title'].indexOf(ville)>-1){
          longitude=result["features"][i]["geometry"]['coordinates'][0][0][0][0];
          latitude=result["features"][i]["geometry"]['coordinates'][0][0][0][1];
        };
        
        }
        
        if (latitude != '' || longitude != ''){
          map.setView(new View({
            center: fromLonLat([parseFloat(longitude),parseFloat(latitude)]),
            zoom: 9.5
          }));

        }
        else{
          window.alert("Veuillez sélectionner une localité");
        }
    })
  }
 }

}