/** map.service.ts
 * Service pour la gestion de la carte et des fonds de carte
 * 
 * Lubin ROINEAU , Vincent HEAU , Jacqueline WILLIAMS, Maxime BONNET 
 * Mai 2022
 */

// _______________________  Importation des modules __________________


import { Injectable } from '@angular/core';

import { View, Map } from 'ol';
import Tile from 'ol/layer/Tile';
import { Point } from 'ol/geom';
import { fromLonLat } from 'ol/proj';
import FullScreen from 'ol/control/FullScreen';

import TileLayer from 'ol/layer/Tile';

import { register } from 'ol/proj/proj4.js'
import * as proj4x from 'proj4'
import Projection from 'ol/proj/Projection';
import { get as GetProjection, transform } from 'ol/proj';
import {Feature} from 'ol';
import { TileWMS, Vector as SourceVector} from 'ol/source';
import {Layer, Vector} from 'ol/layer';
import {format} from 'ol/coordinate';
import { ScaleLine, defaults as DefaultControls, MousePosition } from 'ol/control';

import OSM from 'ol/source/OSM';

import XYZ from 'ol/source/XYZ';

import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import WMTS from 'ol/source/WMTS';
import WMTSTileGrid from 'ol/tilegrid/WMTS';
import {get as getProjection} from 'ol/proj';
import {getWidth} from 'ol/extent';

import {GeoJSON} from 'ol/format';

// Constante de définition des couches 
const proj4 = (proj4x as any).default
const resolutions = [];
const matrixIds = [];
const proj3857 = getProjection('EPSG:3857');
const maxResolution = getWidth(proj3857.getExtent()) / 256;

for (let i = 0; i < 20; i++) {
  matrixIds[i] = i.toString();
  resolutions[i] = maxResolution / Math.pow(2, i);
}
const tileGrid = new WMTSTileGrid({
  origin: [-20037508, 20037508],
  resolutions: resolutions,
  matrixIds: matrixIds,
});



// Voir ce que ça fait vraiment


@Injectable({
  providedIn: 'root'
})



/*Début de la classe MapService*/

export class MapService {

  
  
  // Variable de la carte
  public map: Map;

  // Ensemble des couches 
  public OSM : Layer;
  public OPENTOPO : Layer;
  public IGNTOPO : Layer ;
  public WDS :Layer;
  public MASSIF: Layer;
  public FSC_TOC: Layer;
  public SWS : Layer;
  public S2L2A : Layer;
  public SENTIER : Layer;
  public PENTE : Layer ;

  public MARKERS : Layer ; 

  public projection: Projection;
  public datechoisie;
  

  // Constructeur
  constructor() {

    proj4.defs("EPSG:3857", "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs");
    register(proj4);
    
    // Creation de la date du jour
    this.datechoisie = new Date();
    var jour = this.datechoisie.getDate();
    var mois = this.datechoisie.getMonth()+1; 
    var annee = this.datechoisie.getFullYear();

    this.datechoisie = annee + "-" + mois + "-" + jour;

    /* ------- Couche OSM basique ------- */
    this.OSM = new Tile({
      source: new OSM()
    }),

    /* ------- Couche OpenTopoMap ------- */
    this.OPENTOPO = new TileLayer({
      source: new XYZ({
        url: 'https://{a-c}.tile.opentopomap.org/{z}/{x}/{y}.png',
        attributions: 
          'map data: ©<a href="https://openstreetmap.org/copyright">OpenStreetMap</a> Contributors, <a href="http://viewfinderpanoramas.org">SRTM</a> | map style: ©<a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
      })
    }),
    

    /* ------- Couche WDS ------- */
    this.WDS =  new TileLayer({
      source: new TileWMS({
        url :'https://cryo.land.copernicus.eu/wms/WDS/',
        crossOrigin: 'anonymous',
        params: {'LAYERS': '1_SSC',
                  'TIME': this.datechoisie},
      }),
    });

    /* Couche des massifs (format geojson) */
    this.MASSIF = new VectorLayer({
      source: new VectorSource({
          //url: "https://api.jsonbin.io/b/627f777b019db467969e14be",
          url : ".assets/Massifs_France.geojson",
          format: new GeoJSON()
          
      }),
      visible: true
});



    /* ------- Couche FSC_TOC ------- */
    this.FSC_TOC = new TileLayer({
      source: new TileWMS({
        url :'https://cryo.land.copernicus.eu/wms/FSC/',
        crossOrigin: 'anonymous',
        params: {'LAYERS': '1_FSC_TOC'},
      }),
    });

    /* ------- Couche SAR WET Snow ------- */
    this.SWS = new TileLayer({
      opacity: 0.4,
      source: new TileWMS({
        url :'https://cryo.land.copernicus.eu/wms/SWS/',
        crossOrigin: 'anonymous',
        params: {'LAYERS': '1_WSM',
                  'TIME': '2022-4-30'},
      }),
    });

    /* ------- Couche TRUE-COLOR-S2L2A ------- */
    //Attention à éclaircir, voir issues Gitlab
    this.S2L2A = new TileLayer({
      source: new TileWMS({
        url: 'http://services.sentinel-hub.com/ogc/wms/bfb7dde5-e9a7-47c0-8ddc-980b7e0a928e',
        crossOrigin: 'anonymous',
        params: {'LAYERS': 'TRUE-COLOR-S2L2A',},
      }),
    });

    /* ------- Couche des sentiers OSM ------- */
    this.SENTIER = new TileLayer({
      source: new TileWMS({
        url: 'https://magosm.magellium.com/geoserver/ows?SERVICE=WMS&',
        crossOrigin: 'anonymous',
        params: {'LAYERS': 'magosm:france_hiking_foot_routes_line',},
      }),
    });

    /* ------- Carte des pentes de l'IGN ------- */
    this.PENTE = new TileLayer({
      opacity: 0.4,
      source: new WMTS({
        url: 'https://wxs.ign.fr/an7nvfzojv5wa96dsga5nk8w/geoportail/wmts?',
        layer: 'GEOGRAPHICALGRIDSYSTEMS.SLOPES.MOUNTAIN',
        matrixSet: 'PM',
        format: 'image/png',
        projection: 'EPSG:3857',
        tileGrid: tileGrid,
        style: 'normal',
        attributions:
          '<a href="https://www.ign.fr/" target="_blank">' +
          '<img src="https://wxs.ign.fr/static/logos/IGN/IGN.gif" title="Institut national de l\'' +
          'information géographique et forestière" alt="IGN"></a>',
      }),
    });


    /* ------- Carte topographique de l'IGN ------- */
    this.IGNTOPO = new TileLayer({
      opacity: 1,
      source: new WMTS({
        url: 'https://wxs.ign.fr/choisirgeoportail/geoportail/wmts',
        layer: 'GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2',
        matrixSet: 'PM',
        format: 'image/png',
        projection: 'EPSG:3857',
        tileGrid: tileGrid,
        style: 'normal',
        attributions:
          '<a href="https://www.ign.fr/" target="_blank">' +
          '<img src="https://wxs.ign.fr/static/logos/IGN/IGN.gif" title="Institut national de l\'' +
          'information géographique et forestière" alt="IGN"></a>',
      }),
    });

   }

  
   // Création initiation de la carte

   public initMap(){
    var self = this;

    this.projection = GetProjection('EPSG:3857');
     
    /*Ajout de la carte
    La paramètre layers permet de sélectionner le layer à choisir. 
    Différents types de layers sont proposés
    */ 
    var mousePositionControl = new MousePosition({
      coordinateFormat: function (coords) {
        return format(coords, 'Lat : {y}° Lon : {x}° (WGS84)', 4)
      },
      projection: 'EPSG:4326',
      className: 'mouse-position',
      target: document.getElementById('mouse-position'),
      undefinedHTML: '&nbsp;'
    });

    this.map = new Map({
        target: 'map',
        layers: [
           
          // Fonds de carte par défaut lors de l'ouverture du portail
          this.IGNTOPO,
          this.PENTE,
        ],

        // Ajout des vues
        // Centrage sur le Mont-Blanc
        view: new View({
                center:fromLonLat([6.865175,45.832622]),
                zoom:13,
                projection: self.projection
              }),
        controls: DefaultControls().extend([
          mousePositionControl,
          new FullScreen(),
          new ScaleLine({
              bar: false,
              minWidth: 140
                })
          ])       

    });
 
  }

}

