/** geo-tiff-layer.component.ts
 * Ensembles des algorithmes liées à la détection du contour 
 * Ensemble des algorithme liées à l'extraction vectorielle des masques
 * 
 * Lubin ROINEAU , Vincent HEAU , Jacqueline WILLIAMS, Maxime BONNET 
 * Mai 2022
 */

/* Imports */

import { Component, OnInit } from '@angular/core';
import { fromArrayBuffer} from 'geotiff';


import 'ol/ol.css';
import TileLayer from 'ol/layer/WebGLTile';
import {TileWMS } from 'ol/source';

import GeoJSON from 'ol/format/GeoJSON';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import {DragBox, Draw} from 'ol/interaction';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style';
import {altKeyOnly} from 'ol/events/condition';
import { MapService } from 'src/app/services/map.service';
import { Feature } from 'ol';
import { Polygon } from 'ol/geom';


import * as math from 'mathjs';
import * as Highcharts from 'highcharts';



@Component({
  selector: 'app-geo-tiff-layer',
  templateUrl: './geo-tiff-layer.component.html',
  styleUrls: ['./geo-tiff-layer.component.css']
})

/** GeoTiffLayerComponent
 * Algorithmes pour analyser la neige
 *  -- détection de la zone à analyser
 *  -- traitement sur les pixels avec GeoTiff
 *  -- Création d'une couche vectorielle (format GeoJSON)
 */
export class GeoTiffLayerComponent implements OnInit {
  
  // Initialisation des variables liées à l'analyse de la neige
  public dateNeige: Date;

  public couche_GEOJSON;
  public couche_wds;

  constructor(public mapService: MapService ) { 
    this.dateNeige = new Date();
  }


  /**
     * Suppression de la carte des couches de neige (neige fraiche ou neige humide)
     * @param {String} type_neige Le type de neige, fraiche ou humide
    */
  fermeture_couche(type_neige){

    for (let i = 0; i < this.mapService.map.getAllLayers().length; i++) {
      if (type_neige == this.mapService.map.getAllLayers()[i].get('name')){
        this.mapService.map.removeLayer(this.mapService.map.getAllLayers()[i]);
      }
    }
    
  }
  
  /**
     * Méthode qui se lance lors du clic sur les boutons d'analyse de la neige
     * Elle initialise la vue sur la carte et lance les fonctions de calcul
     * Ces dernières sont définies en dehors de la classe
     * @param {Map} type_neige Le type de neige, fraiche ou humide
     * @param {Number} type Le type de neige, fraiche ou humide 
     *                         0 neige fraiche
     *                         1 neige humide 
    */
  public selection(map,type){

    /* Affichage et Initialisation */

    // Mise à jour de la date
    var jour = this.dateNeige.getDate();
    var mois = this.dateNeige.getMonth()+1;
    var annee = this.dateNeige.getFullYear();

    var date_choisie = annee + "-" + mois + "-" + jour;
    

    // Gestion de l'action des boutons
    var bouton_neige_humide = <HTMLButtonElement>document.getElementById('text-image-2');
    var bouton_neige_fraiche = <HTMLButtonElement>document.getElementById('text-image');
    var bouton_itineraire = <HTMLButtonElement>document.getElementById('text-bouton');

    bouton_neige_humide.disabled = true ;
    bouton_neige_fraiche.disabled = true ;
    bouton_itineraire.disabled = true ;
    
    // Interaction avec la carte qui se met en place lors du clique sur le bouton
    var draw = new Draw({
      source: new VectorSource(),
      type: 'Point',
      style: new Style({
        fill: new Fill({
          color: 'rgba(0,0,0, 0)',
        }),
        stroke: new Stroke({
          color: 'rgba(0,0,0,0)',
        }),
        image: new CircleStyle({
          radius: 7,
          stroke: new Stroke({
            color: 'rgba(255,0,0, 0.9)',
          }),
          fill: new Fill({
            color: 'rgba(255, 0, 2, 0.4)',
          }),
        }),
      }),
    });


    map.addInteraction(draw);
    

    // Appuyer sur le bouton alt pour afficher la DragBox
    var dragBox = new DragBox({
      condition: altKeyOnly,
    });
      
    map.addInteraction(dragBox);
    

    /* Initialisation pour le lancement des calculs au moment où la sélection est terminée */

    dragBox.on('boxend', function(e) {

        // Affichage du chargement (barre de chargement)
        document.getElementById('chargement').style.display='block';

        //Gestion des boutons
        bouton_neige_humide.disabled = false ;
        bouton_neige_fraiche.disabled = false ;
        bouton_itineraire.disabled = false ;

        // Partie 1 : Récupération de la BBOX (dans le sens des aiguilles d'une montre)
        var coin1= dragBox.getGeometry().getExtent()[0];
        var coin11= dragBox.getGeometry().getExtent()[1];
        var coin2= dragBox.getGeometry().getExtent()[2];
        var coin22= dragBox.getGeometry().getExtent()[3];
        
        // céation du polygone de sélection
        const polygone = new Feature(
          new Polygon([
            [
              [coin1,coin11],
              [coin2,coin11],
              [coin2,coin22],
              [coin1,coin22],
            ],
          ])
        );

        var sourcepoly = new VectorSource({features: [polygone]});
        var vecteurpoly = new VectorLayer({
          source: sourcepoly,
          style :new Style({
            stroke: new Stroke({
              color: 'rgba(175, 60, 225, 1)',
              width: 3,
            }),
            fill: new Fill({
              color: 'rgba(0, 0, 0, 0)',
            }),
          }),
        });
        
        /* Commande permettant de voir le geotiff sur lequel se fait l'extraction, utile pour le debug
        // Variable non utilisée, permettant d'afficher la couche WDS sur la zone si nécessaire
        var source_WDS = new TileWMS({
          url: 'https://cryo.land.copernicus.eu/wms/WDS/',
          params: {'LAYERS': '1_SSC',
                  'TIME': date_choisie},
        })
        */
        
        // Affichage du rectangle de sélection
        map.addLayer(vecteurpoly);
        
        // On récupère l'image geotiff avec les paramètres désirés
        var imgtiff = creation_lien_tiff([coin1, coin11, coin2, coin22],date_choisie);
        
        // Suppression des interactions avec la carte
        map.removeInteraction(dragBox);   
        map.removeInteraction(draw);   
        var l = Math.abs(coin2-coin1);
        var L = Math.abs(coin22-coin11);

        if (L>14000 || l>14000){
          document.getElementById('erreur_zone').style.display='block';
          document.getElementById('chargement').style.display='none';
          map.removeLayer(vecteurpoly);
        }
        else{
          // Traitement et affichage du geoJSON crée (masque vectoriel)
          
          loadImage(imgtiff,map,vecteurpoly,type);
        }
          
        
      });
  }

    ngOnInit(): void {
  }

}


////////////////////////////////////////////////////////////////////////////////
///     Fonction de traitement pour extraction vectorielle des masques       ///
////////////////////////////////////////////////////////////////////////////////

 /**
   * Fonction asynchrone qui utilise le module node GeoTiff.js 
   * Récupération des valeurs des pixels sur la zone sélectionnée
   * Traitements et calculs extrayant les contours
   * @param {String} filename Le lien de l'image TIFF
   * @param {Map} map La carte sur laquelle on travaille
   * @param {VectorLayer} vecteurpoly Le rectangle de sélection
   * @param {Number} type Le type de neige, fraiche ou humide
   *                      0, neige fraîche
   *                      1, neige humide
  */
var loadImage = async function (filename,map,vecteurpoly,type) {

  /* Commande pour debug */
  // console.log('Début du traitement');
  
  /* Il est possible que le lien de l'image GeoTIFF ne soit pas valide 
  Nécessité donc d'un bloc try{} catch{} pour répondre à cette éventualité */

  try{
    var xhr = new XMLHttpRequest();
    xhr.open("GET", filename);
    xhr.responseType = "arraybuffer";
    xhr.onload = async function (e) {
      var buffer = xhr.response;
      
      // Extraction des constantes nécessaires dans l'image
      const tiff = await fromArrayBuffer(buffer);
      const image = await tiff.getImage();
      const bbox = image.getBoundingBox();
      const data = await image.readRasters();
      
      /* Il est possible que le radar ne soit pas passé à cet endroit 
      L'image renvoyée ne comporte alors pas les éléments nécessaires 
      à l'extraction et à la polygonalisation 

      Nécessité donc d'un bloc try{} catch{} pour répondre à cette éventualité */
      try{
        var l = bbox[2]-bbox[0];
        var L = bbox[3]-bbox[1];

        if (L>14000 || l>14000){
          document.getElementById('erreur_zone').style.display='block';
          map.removeLayer(vecteurpoly);
        }

        else{
          
          // On crée la matrice avec les valeurs des pixels pour chaque pixel
          var liste_pts = extraction_neige(data,type);

          // Création du multi-polygone (coeur de l'algorithme)
          var POLY = polygonalisation(liste_pts,bbox);
          
          // Création d'un GEOJSON contenant ce multipolygone
          var GEOJSON_neige = creation_geojson(POLY);
            
          //map.getAllLayers()[6].setOpacity(0.3);
          
          // Ajout du contour sur la carte
          ajout_contour(map,GEOJSON_neige,type);
          map.removeLayer(vecteurpoly);

          // Pas d'erreur, le traitement a réussi
          document.getElementById('erreur').style.display='none';
          document.getElementById('erreur_zone').style.display='none';

        }
        
      }

      catch{
        // erreur, le traitement (polygonalisation) ne peut pas se faire
        document.getElementById('erreur').style.display='block';   
        map.removeLayer(vecteurpoly);
      }

      
      // La barre de chargement est masquée
      document.getElementById('chargement').style.display='none';

      
    };
    xhr.send();
  }

  // en cas de problème dans le lien, d'erreur Copernicus
  catch{
    alert('Le serveur Copernicus Snow&Ice ne répond pas. Vérifiez votre connection internet ou essayer sur une autre zone à une autre date')
  }
  
};



/**
   * Fonction qui crée le lien permettant d'accéder à l'image Tiff WDS sur une zone sélectionnée
   * @param {number[]} bbox Les coordonnées de la bbox (rectangle de sélection)
   * @param {Date} date La date à laquelle on récupère l'image Tiff
   * @returns{String} lien Le lien avec les bons paramètres
  */
function creation_lien_tiff(bbox: number[],date) {
 
  // Détermination d'un coefficient (1 pixel de l'image par pixel WDS)
  var coeff = 9.55462853564677; // explication du calcul de ce coeff dans la documentation programmeur 
  
  // Calcul des hauteurs et largeurs
  var w = Math.round(Math.abs(bbox[2]-bbox[0])/coeff);
  var h = Math.round(Math.abs(bbox[3]-bbox[1])/coeff);

  var lien = 'https://cryo.land.copernicus.eu/wms/WDS/?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image/tiff&TRANSPARENT=true&LAYERS=1_SSC&TIME='+date+'&WIDTH=' + w + '&HEIGHT=' + h + '&CRS=EPSG:3857&STYLES=&BBOX=' + bbox[0] + ',' + bbox[1] + ',' + bbox[2] + ',' + bbox[3]
  return lien;
}




/**
   * Fonction qui crée le lien permettant d'accéder à l'image Tiff WDS sur une zone sélectionnée
   * @param {import("geotiff").TypedArray | import("geotiff").TypedArray[]} data
   *        L'image TIFF de la zone sélectionnée sous forme de matrice de pixels
   * @param {Number} type Le type de neige, fraiche ou humide
   *                      0, neige fraîche
   *                      1, neige humide
   * @returns{number[]} resulat-contour Une liste des pixels de contour
  */
function extraction_neige(data: import("geotiff").TypedArray | import("geotiff").TypedArray[],type) {
  
  var resultat_contour = []

  var largeur=data["width"];
  var longueur=data["height"];
  
  // Red 
  var array_red = [].slice.call(data[0]);
  // On multiplie par deux (voir documentation programmeur)
  // Petite astuce pour avoir une valeur unique par couleur
  var  x_red = math.multiply(math.matrix(array_red),2);
  var red = math.reshape(x_red,[longueur,largeur]);

  // Green 
  var array_green = [].slice.call(data[1]);
  var  x_green = math.matrix(array_green);
  var green = math.reshape(x_green,[longueur,largeur]);
  
  // Blue 
  var array_blue = [].slice.call(data[2]);
  var  x_blue = math.matrix(array_blue);
  var blue = math.reshape(x_blue,[longueur,largeur]);
  
  var S = math.add(math.add(red,green),blue);
  var T = math.add(math.add(red,green),blue);
  
  // Initialisation des compteurs (diagramme de répartition de la neige)
  var nb_noir = 0;
  var nb_blanc = 0;
  var nb_rose = 0;
  var nb_gris = 0;
  var nb_bleu = 0;
  var nb_vert = 0;
  var nb_orange = 0;
  var nb_jaune = 0;


  // comptage des pixels
  for (let i = 0; i < longueur; i++) {
    for (let j = 0; j < largeur; j++) {

      if (S.get([i,j])==0){
        // Le pixel est noir 
        nb_noir+=1; 
      }

      else if (S.get([i,j])==770){
        // Le pixel est jaune
        nb_jaune+=1;
      }

      else if (S.get([i,j])==245){
        // Le pixel est vert
        nb_vert+=1;
      }

      else if (S.get([i,j])==415){
        // Le pixel est bleu
        nb_bleu+=1;
      }

      else if (S.get([i,j])==118){
        // Le pixel est gris
        nb_gris+=1;
      }

      else if (S.get([i,j])==800){
        // Le pixel est orange
        nb_orange+=1;
      }
      
      // condition 1 : Le pixel est rose
      // Traitement particulier car il s'agit de la neige humide
      else if (S.get([i,j])==765){
        // Le pixel est rose
        nb_rose+=1;
        if (type==1){
          // condition 2 : Il n'est pas sur le contour
          if (i>0 && j>0 && i<longueur-1 && j<largeur-1){
            //condition 3 : il est sur un contour
            if (voisins(i,j,S)<7){
              T.set([i,j],111);
              resultat_contour.push({ pixel: [i,j], x: i, y: j });
            }
          }
          // condition 2bis : si contour + 0
          else if (i==0 || j==0 || i==longueur-1 || j==largeur-1){
            T.set([i,j],111);
            resultat_contour.push({ pixel: [i,j], x: i, y: j});

          }
        }
      }

      // condition 1 : Le pixel est blanc
      // Traitement particulier car il s'agit de la neige fraîche
      else if (S.get([i,j])==1020){
        nb_blanc+=1
        if (type==0){
          // condition 2 : Il n'est pas sur le contour
          if (i>0 && j>0 && i<longueur-1 && j<largeur-1){
            //condition 3 : il est sur un contour
            if (voisins(i,j,S)<7){
              T.set([i,j],111);
              resultat_contour.push({ pixel: [i,j], x: i, y: j });
            }
          }
          // condition 2bis : si contour + 0
          else if (i==0 || j==0 || i==longueur-1 || j==largeur-1){
            T.set([i,j],111);
            resultat_contour.push({ pixel: [i,j], x: i, y: j});

          }
        }
      }
    }
  }

  // Mise à jour des variables 
  var total = nb_blanc + nb_noir + nb_bleu + nb_rose + nb_jaune + nb_orange + nb_vert + nb_gris;
  
  var pp_neigefraiche = Math.round(nb_blanc*100/total);
  var pp_neigehumide = Math.round(nb_rose*100/total);
  var pp_absenceneige = Math.round(nb_jaune*100/total);
  var pp_ombre = Math.round(nb_noir*100/total);
  var pp_ombrenuage = Math.round(nb_gris*100/total);
  var pp_eau = Math.round(nb_bleu*100/total);
  var pp_foret = Math.round(nb_vert*100/total);
  var pp_urbain = Math.round(nb_orange*100/total);
  
  // légende du graphique
  var repartition = [
    {name:'Neige humide',y:   pp_neigehumide, color :'#FF1493'},
    {name:'Neige fraîche',y:   pp_neigefraiche, color :'white'},
    {name:'Absence de neige ou névés',y: pp_absenceneige, color :'yellow'},
    {name:'Ombre du radar',y: pp_ombre, color :'black'},
    {name:'Zone nuageuse',y: pp_ombrenuage, color :'grey'},
    {name:'Eau',y: pp_eau, color :'blue'},
    {name:'Forêt',y: pp_foret, color :'green'},
    {name:'Zone Urbaine',y: pp_urbain, color :'orange'},
  ];
  
  // condition indispensable pour ne pas afficher un diagramme tout noir sur une zone sans données
  if (total != nb_noir){
    diagramme(repartition);
  }
  return resultat_contour;
}



/**
   * Fonction qui compte, pour une case d'une matrice,
   *  l'ensemble des cases voisines de même valeur
   *
   * @param {Number} i indice de la ligne
   * @param {Number} j indice de la colonne
   * @param {math.Matrix} S Matrice comportant les valeurs des pixels
   * @returns{Number} n Nombre de voisins identiques
   *                    De 0 pour une case isolée
   *                    A 8 pour une case au milieu d'autres identiques
  */
function voisins(i: number,j: number, S: math.Matrix) {
  var n = 0
  for (let p = i-1 ; p < i+2; p++) {
    for (let q = j-1; q < j+2; q++) {
      if (S.get([i,j])==S.get([p,q]) && [p,q]!=[i,j]){
          n+=1
      }
    }
  }
  return n
}

/**
   * Fonction au coeur du processus de création des contours,
   * La liste de points du contour est transformée en un tableau de polygones
   * (format geojson multipolygone)
   *
   * @param {Number[]} bbox Valeurs de la zone de travail permettant le géoréférencement des pixels
   * @param {any[]} liste_pts Liste des points détectés comme points de contour
   * @returns{Number[]} Tableau de polygones (format multipolygone)
  */
function polygonalisation(liste_pts: any[], bbox) {
  
  // Initialisation
  var POLY=[]; // le multipolygone final
  var polygone=[];
  
  
  // Etape 1 : Tri de la liste 
  liste_pts.sort(function(a, b){
    return a.x - b.x;
  });
  
  // Etape 2 : création des polygones 
  var k=0;
  var pivot = liste_pts[k];

  polygone.push(georeference(liste_pts[0]["pixel"],bbox));
  liste_pts.splice(0,1);

  while (liste_pts.length > 0){
    // On place chacun des points dans la liste et on vide la liste ensuite
    
    // On recherche le voisin le plus propice dans la zone
    var meilleur_voisin = cherche_voisin(pivot,liste_pts);
    
    // Si ce voisin n'existe pas, on commence un nouveau polygone
    if (Number.isNaN(meilleur_voisin[0])){
      
      // Les pixels isolés sont supprimés (voir documentation programmeur)
      if (polygone.length>1){
        POLY.push([polygone]);
      }
      
      polygone=[];
      polygone.push(georeference(liste_pts[0]["pixel"],bbox));
      k=0;
      pivot = liste_pts[k];
      liste_pts.splice(k,1);
      
    }
    
    // Si ce voisin existe, on l'ajoute à la liste polygone
    else {
      k+=1;
      polygone.push(georeference(meilleur_voisin[0]["pixel"],bbox));
      pivot = liste_pts[meilleur_voisin[1]];
      liste_pts.splice(meilleur_voisin[1],1);
    }
  }
  
  // Condition pour suppression des pixels isolés
  if (polygone.length>1){
    POLY.push([polygone]);
  }

  
  //tri de la liste selon les longueurs
  POLY.sort(function(a, b){
    return b[0].length - a[0].length;
  });

  return POLY;
}


/**
   * Recherche du voisin le plus propice
   * On examine la liste des points de contour et on trouve le voisin le plus propice 
   * par rapport à un pivot donné
   *
   * @param {Number[]} bbox Valeurs de la zone de travail permettant le géoréférencement des pixels
   * @param {any[]} liste_pts Liste des points détectés comme points de contour
   * @returns{Number[]} Tableau comportant le meilleur voisin du pivot ainsi que son indice
   *                    [NaN, 0] si ce dernier n'existe pas
  */
function cherche_voisin(pivot, liste_pts: any[]) {
  
  // Initialisation du pivot
  var x_pivot = pivot["x"];
  var y_pivot = pivot["y"];

  // le voisin sera un NaN si on ne trouve pas de voisin direct
  var meilleur_voisin = NaN ;
  var indice_meilleur_voisin = 0 ;
  
  // Distance en pixel à ne pas dépasser sans quoi un pixel ne peut pas être le voisin d'un autre
  var dist_min = 10;

  for (let k = 0 ; k < liste_pts.length; k++) {
   
    var x_variant = liste_pts[k]["x"];
    var y_variant = liste_pts[k]["y"];

    // condition pour ne pas prendre le pivot
    if (x_variant != x_pivot || y_variant != y_pivot){
      
      // Préparation de la condition
      var e_x = Math.abs(x_pivot-x_variant);
      var e_y = Math.abs(y_pivot-y_variant);
      
      // C'est un voisin direct, on le renvoie automatiquement
      if ((e_x+e_y) <=1){
        return [liste_pts[k],k];
      } 

      // ce n'est pas un voisin direct, on regarde si c'est malgré tout un voisin
      else if (e_x<=10 && e_y<=10){
        
        if (Math.sqrt(e_x**2+e_y**2)<=dist_min){
          
          dist_min = Math.sqrt(e_x**2+e_y**2);
          meilleur_voisin = liste_pts[k];
          indice_meilleur_voisin = k ;
        }
      }
    }
    
  }
  return [meilleur_voisin, indice_meilleur_voisin];
}

/**
   * Associe à chaque pixel de la zone sélectionnée, ses coordonnées géographiques
   *
   * @param {Number[]} bbox Valeurs de la zone de travail permettant le géoréférencement des pixels
   * @param {Array<2>} pixel tableau comportant les coordonnées du pixel dans la zone
   * @returns{Number[]} Tableau des coordonnées géographiques du pixel
  */
function georeference(pixel: Array<2>, bbox) {

  var taillepx = 9.554628535646998 
  // Coefficient de taille du pixel sur le terrain
  // (voir documentation programmeur)
  var i = pixel[1]*taillepx + bbox[0];
  var j = bbox[3]-pixel[0]*taillepx;

  return [i,j]
}


/**
   * Associe à chaque pixel de la zone sélectionnée, ses coordonnées géographiques
   *
   * @param {Number[]} bbox Valeurs de la zone de travail permettant le géoréférencement des pixels
   * @param {Array<2>} pixel tableau comportant les coordonnées du pixel dans la zone
   * @returns{GeoJSON} g GeoJSON comportant la vectorisation de la zone de neige
   *                     (format multipolygone)
  */
function creation_geojson(POLY: any[]) {

    var g= {
      'type': 'FeatureCollection',
      'crs': {
        'type': 'name',
        'properties': {
          'name': 'EPSG:3857',
        },
      },
      'features': [
        {
          'type': 'Feature',
          'geometry': {
            'type': 'MultiPolygon',
            'coordinates': POLY
          },
        },
      ],
    };

    return g;
}


/**
   * Fonction qui ajoute un GeoJSON sur la carte avec une légende adaptée à son type
   *
   * @param {Map} map La carte en question
   * @param {GeoJSON} geojson Le fichier geojson à ajouter
   * @returns{VectorLayer} vectorLayer La couche OpenLayers vecteur
  */
function ajout_contour(map,geojson,type){
 
  var vectorSource = new VectorSource({
    features: new GeoJSON().readFeatures(geojson),
  });

  // Distinction dans la couleur de remplissage
  if (type == 0) {
    var couleur = 'rgb(8,165,249,0.8)';
  }
  else{
    var couleur = 'rgb(223,39,159,0.8)'
  }

  var vectorLayer = new VectorLayer({
    source: vectorSource,
    style : new Style({
      stroke: new Stroke({
        color: 'blue',
        width: 1.2,
      }),
      fill: new Fill({
        color: couleur,
      }),
    }),
  });
  
  if (type==0){
    vectorLayer.set('name', 'neige_fraiche');
  }
  else{
    vectorLayer.set('name', 'neige_humide');
  }
  
  map.addLayer(vectorLayer);

  return vectorLayer;
}


/**
   * Fonction qui crée le diagramme de répartition de la neige à afficher
   * (sur la zone sélectionnée précédemment)
   *
   * @param {JSON[]} repartition Tableau comportant le Json de la répartition
  */
function diagramme(repartition) {

  // On insère les paramètres désirées
  var options: any = {
    chart : {
      plotBorderWidth: null,
      plotShadow: false
    },
    title : {
        text: 'Composition de la zone sélectionnée'   
    },
    tooltip : {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions : {
        pie: {
          shadow: false,
          center: ['50%', '50%'],
          size:'45%',
          innerSize: '20%'            
        }
    },
    series : [{
        type: 'pie',
        name: "Part de l'itinéraire",
        data: repartition
    }]
    };
    var theme: any= {
      
      chart: {
          backgroundColor: {
              linearGradient: [0, 0, 500, 500],
              stops: [
                  [0, 'rgb(250, 250, 250,0.9)'],
                  [0.5, 'rgb(150, 150, 150,0.9)'],
                  [1, 'rgb(50, 50, 50,0.9)']
              ]
          },
      },
      title: {
          style: {
              color: 'rgb(0, 0, 0)',
              font: 'bold 16px'
          }
      },
      subtitle: {
          style: {
              color: 'rgb(100, 0, 0)',
              font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
          }
      },
      legend: {
          itemStyle: {
              font: '9pt Trebuchet MS, Verdana, sans-serif',
              color: 'black'
          },
          
      }
  };
    Highcharts.setOptions(theme);
    Highcharts.chart('doughnutchart', options);
    
}





