import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeoTiffLayerComponent } from './geo-tiff-layer.component';

describe('GeoTiffLayerComponent', () => {
  let component: GeoTiffLayerComponent;
  let fixture: ComponentFixture<GeoTiffLayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeoTiffLayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeoTiffLayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
