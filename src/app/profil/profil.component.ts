/** profil.component.ts
 * Gestion de l'affiche du graphique et du dessin du profil altimétrique
 * Ensemble des algorithmes liées à l'interpolation
 * Ensemble des algorithmes liées à la connection au service de MNT de l'IGN
 * 
 * Lubin ROINEAU , Vincent HEAU , Jacqueline WILLIAMS, Maxime BONNET 
 * Mai 2022
 */

/* Imports */

import { Component, OnInit,} from '@angular/core';
import { Chart } from 'chart.js';
import * as Highcharts from 'highcharts';
import Draw from 'ol/interaction/Draw';
import { fromLonLat, toLonLat } from 'ol/proj';
import VectorSource from 'ol/source/Vector';
import { Style } from 'ol/style';
import CircleStyle from 'ol/style/Circle';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import {getDistance} from 'ol/sphere';

import { MapService } from 'src/app/services/map.service';

// déclaration d'une constante pour Openlayers (gestion des couches)
const source = new VectorSource();

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})

/** ProfilComponent
 * Algorithmes pour tracer le profil
 *  -- détection ddu chemin via dessin Openlayers
 *  -- Interpolation des points sur l'ensemble du chemin
 *  -- Connection au service MNT de l'IGN pour avoir l'altimétrie des points 
 *  -- Détection des points dans des zones de neige fraîche
 */

export class ProfilComponent implements OnInit {
  
  // Variables comportant toutes les options du graphiques 
  public options: any  = {
    chart: {
      type: 'area',
      zoomType: 'x',
      panning: true,
      panKey: 'shift',
      scrollablePlotArea: {
          minWidth: 400
      }
    },

    caption: {
        text: 'Profil altimétrique de la course choisie : Si une analyse de la neige a été réalisée auparavant, les points blancs représentent des zones de neige fraîche',
    },

    title: {
        text: 'Profil '
    },

    lang: {
        accessibility: {
            screenReaderSection: {
                annotations: {
                    descriptionNoPoints: '{annotationText}, at distance {annotation.options.point.x}km, elevation {annotation.options.point.y} meters.'
                }
            }
        }
    },

    credits: {
        enabled: false
    },

    annotations: [{
        draggable: '',
        labelOptions: {
            backgroundColor: 'rgba(255,255,255,0.5)',
            verticalAlign: 'top',
            y: 15
        },
        labels: [],
    }, {
        draggable: '',
        labelOptions: {
            shape: 'connector',
            align: 'right',
            justify: false,
            crop: true,
            style: {
                fontSize: '0.8em',
                textOutline: '1px white'
            }
        },
        labels: []
    }],

    xAxis: {
        labels: {
            format: '{value}'
        },
        minRange: 0,
        title: {
            text: 'Distance'
        }
    },

    yAxis: {
        startOnTick: true,
        endOnTick: false,
        maxPadding: 0.35,
        title: {
            text: null
        },
        labels: {
            format: '{value} m'
        },
        accessibility: {
            description: 'Elevation',
            rangeDescription: 'Range: 0 to 4,810 meters'
        }
    },

    tooltip: {
        headerFormat: 'Distance: {point.x:.1f} km<br>',
        pointFormat: '{point.y} mètres',
        shared: true
    },

    legend: {
        enabled: false
    },

    series: [{
        data: [],
        lineColor: Highcharts.getOptions().colors[1],
        color: Highcharts.getOptions().colors[5],
        fillOpacity: 0.5,
        name: 'Elevation',
        marker: {
            enabled: true
        },
        threshold: null
      },
      {
        data: [],
        lineColor: Highcharts.getOptions().colors[1],
        color: Highcharts.getOptions().colors[3],
        fillOpacity: 1,
        name: 'Elevation',
        marker: {
            enabled: false
        },
        threshold: null
      }
    ],

  }
  
  
  public chart : Chart;

  constructor(private mapService: MapService) { 
  }

  /**
   * Fonction qui crée le profil une fois le dessin sur la carte terminé
   * Gestion de l'affichage utilisateur
   * @param {Map} map carte sur laquelle le tracé s'effectue
   * @param {JSON} option Ensemble des options du graphique de profil altimétrique
   * 
  */
  public profil(map,options) {

    var alti = document.getElementById("bouton-profil");
    var info = document.getElementById("info-profil");
    var increment = 0;
   
    // Clique sur le bouton 'Tracer mon itinéraire'
    alti.addEventListener("click",function(){
      
      increment = (increment+1)%2 //passer de 0 à 1 sans cesse

      // Gestion de l'action des boutons
      var bouton_neige_humide = <HTMLButtonElement>document.getElementById('text-image-2');
      var bouton_neige_fraiche = <HTMLButtonElement>document.getElementById('text-image');
      
      bouton_neige_humide.disabled = true ;
      bouton_neige_fraiche.disabled = true ;
      

      if (increment==0){
        map.getView().setZoom(13);
        
        info.style.display = "none";
        document.getElementById('text-bouton').innerText = 'Tracer mon itinéraire';

        // Réactivation des boutons
        bouton_neige_humide.disabled = false ;
        bouton_neige_fraiche.disabled = false ;

        map.removeInteraction(map.getInteractions()["array_"][9]);
        
      }
      else{
        map.getView().setZoom(14);
        info.style.display = "block";
        document.getElementById('text-bouton').innerText = "Masquer l'itinéraire";
        
        
        // Gestion des paramètres du dessin
        var draw = new Draw({
          source: source,
          type: 'LineString',
          style: new Style({
            fill: new Fill({
              color: 'rgba(255, 0, 0, 1)',
            }),
            stroke: new Stroke({
              color: 'rgba(200,34, 200, 1)',
              lineDash: [15, 5],
              width: 3,
            }),
            image: new CircleStyle({
              radius: 7,
              stroke: new Stroke({
                color: 'rgba(0,0, 0, 0.9)',
              }),
              fill: new Fill({
                color: 'rgba(255, 0, 255, 0.4)',
              }),
            }),
          }),
        });
      map.addInteraction(draw);

      
        // Action qui s'effectuent lorsque l'utilisateur a fini de dessiné sur la carte
        draw.on('drawend', function(event){

          var liste_points = map.getInteractions()["array_"][9]["sketchCoords_"];

          // Création de la liste du profil avec la liste des points
          var courbe_profil = [];
          var lon='';
          var lat='';

          // Interpolation tous les 100 m
          liste_points=interpolation(liste_points, 100 );
          
          // Préparation de la liste pour la requête de l'IGN
          for(var i = 0; i < liste_points.length; i++){
            var longi=0.0;
            var lati=0.0;

            [longi,lati]=[liste_points[i][0],liste_points[i][1]];
            if (i == liste_points.length-1){
              lon +=longi;
              lat +=lati;
            }
            else{
              lon +=longi+'|';
              lat +=lati+'|';}
          }
          
          /* Nécessité d'utiliser un bloc try{}catch{} permettant de gérer une erreur lié à l'appel au service MNT de l'IGN */
          try{
            // Changement des options 
            options["series"][0]["data"]=mnt_IGN(lon,lat,liste_points.length,courbe_profil,liste_points,map);
            
            // Affichage du graphe dans la div correspondante
            Highcharts.chart('linealti', options);
          }
          catch{
            alert("La connexion au serveur de l'IGN est impossible. Vérifiez votre connexion internet");
          }
          
          map.removeInteraction(draw);
          
        });
      }
    });  
   }

  ngOnInit(): void {
    this.profil(this.mapService.map, this.options);
  }

}


/**
   * Appel du service MNT de l'IGN
   * Retour avec l'atimétrie des points données en entrée
   * 
   * Gestion de l'intersection du point avec les couches de neige fraîche activées
   * @param {Number} lon Longitude du premier point
   * @param {Number} lat Latitude du premier point
   * @param {Number} nb_pt Nombre de points
   * @param {Map} map carte sur laquelle le tracé s'effectue
   * @param {Map} map carte sur laquelle le tracé s'effectue
   * @param {JSON} option Ensemble des options du graphique de profil altimétrique
   * 
  */
function mnt_IGN(lon,lat,nb_pt,courbe_profil,liste_points,map) {
  

  // Récupération de l'URL en question de l'IGN
  var url='https://wxs.ign.fr/essentiels/alti/rest/elevation.json?lon='+lon+'&lat='+lat+'&zonly=true';
  

  // Récupérer l'altitude sans passer par un fetch
  var reponse = new XMLHttpRequest();
  
  reponse.open('GET', url, false);
  reponse.send(null);
  var compteur=0;
  if(reponse.status == 200){

    for (var u=0;u<nb_pt;u++){

    var alt = JSON.parse(reponse.response)["elevations"][u];
    if(u>1){
    compteur+=getDistance([liste_points[u][0],liste_points[u][1]],[liste_points[u-1][0],liste_points[u-1][1]]);
    }
    
    // Initialisation de la variable contenant l'indice de la dernière couche de neige fraiche
    var indice_couche_nf = -1;
    for (let i = 0; i < map.getAllLayers().length; i++) {
      if ('neige_fraiche' == map.getAllLayers()[i].get('name')){
        indice_couche_nf =i;
      } 
    }

    if (indice_couche_nf != -1){
      //Dernière couche de neige fraîche
      var nf = map.getAllLayers()[indice_couche_nf].getSource().getFeatures()[0].getGeometry();
      
      //Reconstruction de la géométrie exacte
      // condition si point de neige fraîche
      if (nf.intersectsCoordinate(fromLonLat(liste_points[u]))){
        
        courbe_profil[u] ={
          x: compteur/1000,
          y :alt,
          marker: {
            radius: 7,
            fillColor:'white',
            lineColor: 'white',
            lineWidth: 1
        },
        }
      }
      else {
        courbe_profil[u]=[compteur/1000,alt];
      }
    }
    else {
      courbe_profil[u]=[compteur/1000,alt];
    }
    
    
    
    }
    
    return courbe_profil;
  }
  else{
    alert( "La connexion à l'URL de l'IGn n'est pas possible, vérifiez votre connexion internet");
    throw new Error("URL IGN en travaux.... à voir ce qu'on met");
    // mettre une exception pour dire que l'url de l'IGN beug
  }
}

function interpolation(liste_points, distance_pt) {

    var liste_interpolee=[];
    for(let nb=0;nb<liste_points.length-1;nb++){
      liste_interpolee.push(toLonLat(liste_points[nb]));
      
      var coord1=toLonLat(liste_points[nb]);
      var coord2=toLonLat(liste_points[nb+1]);
      var dist=getDistance([coord1[0],coord1[1]],[coord2[0],coord2[1]]);

      if(dist>distance_pt && liste_points.length>1 ){
        var nb_points=~~(dist/distance_pt);
        for (let k=1;k<nb_points+1;k++){
          liste_interpolee.push([    coord1[0]+(coord2[0]-coord1[0])*(k/(nb_points+1))     ,        coord1[1]+(coord2[1]-coord1[1])*(k/(nb_points+1))   ]);
          
        }
      }
    }
    var derniere_valeur=liste_points.length;
    liste_interpolee.push(toLonLat(liste_points[derniere_valeur-1]));
    
    
    return liste_interpolee;
}