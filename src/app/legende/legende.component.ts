/** legende.component.ts
 * Gestion des paramètres basiques du menu d'affichage de gauche
 * 
 * Lubin ROINEAU , Vincent HEAU , Jacqueline WILLIAMS, Maxime BONNET 
 * Mai 2022
 */

/* Imports */
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-legende',
  templateUrl: './legende.component.html',
  styleUrls: ['./legende.component.css']
})

/** LegendeComponent
 *  Gestion de l'affichage de la boîte à outil de gauche
 */
export class LegendeComponent implements OnInit {
  
  // Paramètre de la légende
  title: string;
  description: string;
  createdDate: Date;

  constructor() { }
  
  ngOnInit() {
    this.title = 'Legende';
    this.description = 'Gestion des différentes couches';
    this.createdDate = new Date();
    
  }

  /**
     * Affichage du menu
  */
  AfficheMenu() {

    var x = document.getElementById("menu");
    var y = document.getElementById("container");
    
    if(x.style.display=="block") {
        x.style.display = "none";
        y.classList.toggle("change");
    } else {
        x.style.display = "block";
        y.classList.toggle("change");
    }
  }
}


