import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MapComponent } from './components/map/map.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { LegendeComponent } from './legende/legende.component';

import { ProfilComponent } from './profil/profil.component';

import { GeoTiffLayerComponent } from './geo-tiff-layer/geo-tiff-layer.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MapComponent,
    LegendeComponent,
    ProfilComponent,
    GeoTiffLayerComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NoopAnimationsModule,
    MatNativeDateModule,
    MatDatepickerModule

  ],
  exports: [],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
