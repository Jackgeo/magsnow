<img src="https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/logo.png" alt="MagSnow" width="150"/>

# MagSnow

## MagSnow, qu'est-ce que c'est ?

[MagSnow](https://magsnow.azurewebsites.net) est un portail WEB exploitant principalement les données du service Copernicus Snow&Ice, destiné aux usagers de la montagne. Il permet de tracer son itinéraire, y visualiser la qualité de la neige, et obtenir des informations sur les avalanches, afin qu'un pratiquant de ski ou un randonneur puisse préparer sa sortie.

![MagSnow screenshot](https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/MagSnowRelease.png)

## Documentation et exemples

Le guide d'utilisation de MagSnow est disponible [ici](guide_utilisation.md). Il contient un tutoriel permettant de prendre en main rapidement les différentes fonctionnalités offertes par le portail. 

Un onglet `Aide` est également disponible sur le site. 

Un exemple vidéo peut être regardé [ici](https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/vidéo_presentation.wmv). 

## Comment le faire tourner en local ?

### Prérequis 

Afin de faire tourner MagSnow en local, plusieurs intallation sont requises.
- Installer [Node.js](https://nodejs.org/en/download/)
- Installer [Angular](https://angular.io/). Pour cela, après avoir installé Node, ouvrez une invite de commande, et tapez 

```
npm install -g @angular/cli
```

### Installation 

Vous pouvez maintenant déployer MagSnow localement sur votre machine. Pour cela cloner le répertoire git.
- Si vous êtes familier de git, vous pouvez cloner directement depuis la console git :
```
git clone https://gitlab.com/Jackgeo/magsnow.git
```
- Sinon, téléchargez-le (`download-zip`) avec l'onglet à gauche de `Clone`, et l'extraire dans un dossier de votre choix. **Cette opération prend du temps**.

- Pour lancer le serveur, ouvrez une invite de commande, et placez-vous dans votre dossier:
```
cd _chemin-de-votre-dossier_
```
- Puis tapez la commande suivante
```
ng serve
```
Il est possible que l'on vous demande d'installer les modules Node. Dans ce cas, lancez la commande
```
npm install
```
Vous pouvez lancer le serveur local en vous rendant dans votre navigateur et en entrant `localhost:4200`

## Contribution

Si, en tant qu'utilisateur du portail MagSnow, vous avez des idées d'améliorations, de nouveautés, n'hésitez pas à nous contacter en utilisant l'onglet _contact_ du site.

## Auteurs

* **Maxime Bonnet** [Max_Bon](https://gitlab.com/Max_Bon)
* **Vincent Heau** [vincent.heau](https://gitlab.com/vincent.heau)
* **Lubin Roineau** [lubin_roineau](https://gitlab.com/lubin_roineau)
* **Jacqueline Williams** [Jackgeo](https://gitlab.com/Jackgeo)

## Support

MagSnow a été réalisé par 4 étudiants dans le cadre du projet développement de fin de 2ème année de cycle ingénieur à l'ENSG-Géomatique, suite à une demande de l'entreprise Magellium.

MagSnow est actuellement maintenu par [Magellium](https://www.magellium.com/fr/)

<a href="https://www.ensg.eu/">
         <img alt="ENSG" src="https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/logo_ENSG.png" height="100">
</a>
<a href="https://www.magellium.com/fr/">
         <img alt="Magellium" src="https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/Logo_Magellium.png" height="100">
</a>
<a href="https://www.ign.fr">
         <img alt="IGN" src="https://gitlab.com/Jackgeo/magsnow/-/raw/main/src/assets/logo_IGN.png" height="100">
</a>
